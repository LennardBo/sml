## Private repository for the Statistical Machine Learning lecture

This lecture was held by Prof. Ulrike von Luxemburg in SS 2019 and gave a detailed overview of  traditional Machine Learning methods like linear regression, SVMs, PCA or Isomaps.

The weekly exercises were either of mathematical nature or programming tasks done in Jupyter notebooks